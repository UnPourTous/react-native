/**
 * Copyright (c) 2015-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

#import <React/RCTEventEmitter.h>
#import <React/RCTNetworkTask.h>

// ======================= GM Crypto Native start =======================
static NSString *NATIVE_GM_ENCRYPT = @"native-gm-encrypt";
static NSString *GM_ENCRYPT = @"encrypt";
static NSString *SIGNATURE = @"signature";
static NSString *REQUEST_BODY_KEY_STRING = @"string";
static NSString *CONTENT_TYPE_HEADER_NAME = @"content-type";

@protocol WBGMCryptoNativeDelegate<NSObject>
@optional
- (NSString *)wb_SM2SignatureWithContent:(NSString *)source;
- (NSString *)wb_SM4EncryptWithContent:(NSString *)source;
- (NSString *)wb_SM4DecryptWithContent:(NSString *)source;
- (void)wb_reportCustomKeyValueEventProxy:(NSString*)event props:(NSDictionary*)kvs;
- (void)wb_sendTestMessageToTianYan:(NSMutableDictionary *)dic;
@end
// ======================= GM Crypto Native end =======================

@interface RCTNetworking : RCTEventEmitter

/**
 * Does a handler exist for the specified request?
 */
- (BOOL)canHandleRequest:(NSURLRequest *)request;

/**
 * Return an RCTNetworkTask for the specified request. This is useful for
 * invoking the React Native networking stack from within native code.
 */
- (RCTNetworkTask *)networkTaskWithRequest:(NSURLRequest *)request
                           completionBlock:(RCTURLRequestCompletionBlock)completionBlock;

@end

@interface RCTBridge (RCTNetworking)

@property (nonatomic, readonly) RCTNetworking *networking;

@end
