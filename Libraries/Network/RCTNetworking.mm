/**
 * Copyright (c) 2015-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */


#import <mutex>

#import <React/RCTAssert.h>
#import <React/RCTConvert.h>
#import <React/RCTEventDispatcher.h>
#import <React/RCTLog.h>
#import <React/RCTNetworkTask.h>
#import <React/RCTNetworking.h>
#import <React/RCTURLRequestHandler.h>
#import <React/RCTUtils.h>

#import "RCTHTTPRequestHandler.h"

typedef RCTURLRequestCancellationBlock (^RCTHTTPQueryResult)(NSError *error, NSDictionary<NSString *, id> *result);

@interface RCTNetworking ()
// ======================= GM Crypto Native start =======================
@property (nonatomic, weak) id<WBGMCryptoNativeDelegate> wbGMCryptoNativeDelegate;
@property (nonatomic, assign) BOOL useNativeEncrypt;
// ======================= GM Crypto Native end =======================
- (RCTURLRequestCancellationBlock)processDataForHTTPQuery:(NSDictionary<NSString *, id> *)data
                                                 callback:(RCTHTTPQueryResult)callback;
@end

/**
 * Helper to convert FormData payloads into multipart/formdata requests.
 */
@interface RCTHTTPFormDataHelper : NSObject

@property (nonatomic, weak) RCTNetworking *networker;

@end

@implementation RCTHTTPFormDataHelper
{
  NSMutableArray<NSDictionary<NSString *, id> *> *_parts;
  NSMutableData *_multipartBody;
  RCTHTTPQueryResult _callback;
  NSString *_boundary;
}

static NSString *RCTGenerateFormBoundary()
{
  const size_t boundaryLength = 70;
  const char *boundaryChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.";

  char *bytes = (char*)malloc(boundaryLength);
  size_t charCount = strlen(boundaryChars);
  for (int i = 0; i < boundaryLength; i++) {
    bytes[i] = boundaryChars[arc4random_uniform((u_int32_t)charCount)];
  }
  return [[NSString alloc] initWithBytesNoCopy:bytes length:boundaryLength encoding:NSUTF8StringEncoding freeWhenDone:YES];
}

- (RCTURLRequestCancellationBlock)process:(NSArray<NSDictionary *> *)formData
                                 callback:(RCTHTTPQueryResult)callback
{
  RCTAssertThread(_networker.methodQueue, @"process: must be called on method queue");

  if (formData.count == 0) {
    return callback(nil, nil);
  }

  _parts = [formData mutableCopy];
  _callback = callback;
  _multipartBody = [NSMutableData new];
  _boundary = RCTGenerateFormBoundary();

  return [_networker processDataForHTTPQuery:_parts[0] callback:^(NSError *error, NSDictionary<NSString *, id> *result) {
    return [self handleResult:result error:error];
  }];
}

- (RCTURLRequestCancellationBlock)handleResult:(NSDictionary<NSString *, id> *)result
                                         error:(NSError *)error
{
  RCTAssertThread(_networker.methodQueue, @"handleResult: must be called on method queue");

  if (error) {
    return _callback(error, nil);
  }

  // Start with boundary.
  [_multipartBody appendData:[[NSString stringWithFormat:@"--%@\r\n", _boundary]
                              dataUsingEncoding:NSUTF8StringEncoding]];

  // Print headers.
  NSMutableDictionary<NSString *, NSString *> *headers = [_parts[0][@"headers"] mutableCopy];
  NSString *partContentType = result[@"contentType"];
  if (partContentType != nil) {
    headers[@"content-type"] = partContentType;
  }
  [headers enumerateKeysAndObjectsUsingBlock:^(NSString *parameterKey, NSString *parameterValue, BOOL *stop) {
    [self->_multipartBody appendData:[[NSString stringWithFormat:@"%@: %@\r\n", parameterKey, parameterValue]
                                dataUsingEncoding:NSUTF8StringEncoding]];
  }];

  // Add the body.
  [_multipartBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
  [_multipartBody appendData:result[@"body"]];
  [_multipartBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];

  [_parts removeObjectAtIndex:0];
  if (_parts.count) {
    return [_networker processDataForHTTPQuery:_parts[0] callback:^(NSError *err, NSDictionary<NSString *, id> *res) {
      return [self handleResult:res error:err];
    }];
  }

  // We've processed the last item. Finish and return.
  [_multipartBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", _boundary]
                              dataUsingEncoding:NSUTF8StringEncoding]];
  NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", _boundary];
  return _callback(nil, @{@"body": _multipartBody, @"contentType": contentType});
}

@end

/**
 * Bridge module that provides the JS interface to the network stack.
 */
@implementation RCTNetworking
{
  NSMutableDictionary<NSNumber *, RCTNetworkTask *> *_tasksByRequestID;
  std::mutex _handlersLock;
  NSArray<id<RCTURLRequestHandler>> *_handlers;
}

@synthesize methodQueue = _methodQueue;

RCT_EXPORT_MODULE()

// ======================= GM Crypto Native start =======================
- (id<WBGMCryptoNativeDelegate>)wbGMCryptoNativeDelegate{
    if (!_wbGMCryptoNativeDelegate) {
        Class gMNativeEncryptImplClass = NSClassFromString(@"GMNativeEncryptImpl");
        if (gMNativeEncryptImplClass) {
            return [[gMNativeEncryptImplClass alloc] init];
        } else {
            return nil;
        }
    }
    return _wbGMCryptoNativeDelegate;
}
// ======================= GM Crypto Native end =======================

- (NSArray<NSString *> *)supportedEvents
{
  return @[@"didCompleteNetworkResponse",
           @"didReceiveNetworkResponse",
           @"didSendNetworkData",
           @"didReceiveNetworkIncrementalData",
           @"didReceiveNetworkDataProgress",
           @"didReceiveNetworkData"];
}

- (id<RCTURLRequestHandler>)handlerForRequest:(NSURLRequest *)request
{
  if (!request.URL) {
    return nil;
  }

  {
    std::lock_guard<std::mutex> lock(_handlersLock);

    if (!_handlers) {
      // Get handlers, sorted in reverse priority order (highest priority first)
      _handlers = [[self.bridge modulesConformingToProtocol:@protocol(RCTURLRequestHandler)] sortedArrayUsingComparator:^NSComparisonResult(id<RCTURLRequestHandler> a, id<RCTURLRequestHandler> b) {
        float priorityA = [a respondsToSelector:@selector(handlerPriority)] ? [a handlerPriority] : 0;
        float priorityB = [b respondsToSelector:@selector(handlerPriority)] ? [b handlerPriority] : 0;
        if (priorityA > priorityB) {
          return NSOrderedAscending;
        } else if (priorityA < priorityB) {
          return NSOrderedDescending;
        } else {
          return NSOrderedSame;
        }
      }];
    }
  }

  if (RCT_DEBUG) {
    // Check for handler conflicts
    float previousPriority = 0;
    id<RCTURLRequestHandler> previousHandler = nil;
    for (id<RCTURLRequestHandler> handler in _handlers) {
      float priority = [handler respondsToSelector:@selector(handlerPriority)] ? [handler handlerPriority] : 0;
      if (previousHandler && priority < previousPriority) {
        return previousHandler;
      }
      if ([handler canHandleRequest:request]) {
        if (previousHandler) {
          if (priority == previousPriority) {
            RCTLogError(@"The RCTURLRequestHandlers %@ and %@ both reported that"
                        " they can handle the request %@, and have equal priority"
                        " (%g). This could result in non-deterministic behavior.",
                        handler, previousHandler, request, priority);
          }
        } else {
          previousHandler = handler;
          previousPriority = priority;
        }
      }
    }
    return previousHandler;
  }

  // Normal code path
  for (id<RCTURLRequestHandler> handler in _handlers) {
    if ([handler canHandleRequest:request]) {
      return handler;
    }
  }
  return nil;
}

- (NSDictionary<NSString *, id> *)stripNullsInRequestHeaders:(NSDictionary<NSString *, id> *)headers
{
  NSMutableDictionary *result = [NSMutableDictionary dictionaryWithCapacity:headers.count];
  for (NSString *key in headers.allKeys) {
    id val = headers[key];
    if (val != [NSNull null]) {
      result[key] = val;
    }
  }

  return result;
}

- (RCTURLRequestCancellationBlock)buildRequest:(NSDictionary<NSString *, id> *)query
                                 completionBlock:(void (^)(NSURLRequest *request))block
{
  RCTAssertThread(_methodQueue, @"buildRequest: must be called on method queue");

  NSURL *URL = [RCTConvert NSURL:query[@"url"]]; // this is marked as nullable in JS, but should not be null
  NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
  request.HTTPMethod = [RCTConvert NSString:RCTNilIfNull(query[@"method"])].uppercaseString ?: @"GET";

  // Load and set the cookie header.
  NSArray<NSHTTPCookie *> *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:URL];
  request.allHTTPHeaderFields = [NSHTTPCookie requestHeaderFieldsWithCookies:cookies];

  // Set supplied headers.
  NSDictionary *headers = [RCTConvert NSDictionary:query[@"headers"]];
  [headers enumerateKeysAndObjectsUsingBlock:^(NSString *key, id value, BOOL *stop) {
    if (value) {
      [request addValue:[RCTConvert NSString:value] forHTTPHeaderField:key];
    }
  }];

  request.timeoutInterval = [RCTConvert NSTimeInterval:query[@"timeout"]];
  request.HTTPShouldHandleCookies = [RCTConvert BOOL:query[@"withCredentials"]];
  NSDictionary<NSString *, id> *data = [RCTConvert NSDictionary:RCTNilIfNull(query[@"data"])];
  NSString *trackingName = data[@"trackingName"];
  if (trackingName) {
    [NSURLProtocol setProperty:trackingName
                        forKey:@"trackingName"
                     inRequest:request];
  }
  return [self processDataForHTTPQuery:data callback:^(NSError *error, NSDictionary<NSString *, id> *result) {
    if (error) {
      RCTLogError(@"Error processing request body: %@", error);
      // Ideally we'd circle back to JS here and notify an error/abort on the request.
      return (RCTURLRequestCancellationBlock)nil;
    }
    request.HTTPBody = result[@"body"];
    NSString *dataContentType = result[@"contentType"];
    NSString *requestContentType = [request valueForHTTPHeaderField:@"Content-Type"];
    BOOL isMultipart = [dataContentType hasPrefix:@"multipart"];

    // For multipart requests we need to override caller-specified content type with one
    // from the data object, because it contains the boundary string
    if (dataContentType && ([requestContentType length] == 0 || isMultipart)) {
      [request setValue:dataContentType forHTTPHeaderField:@"Content-Type"];
    }

    // Gzip the request body
    if ([request.allHTTPHeaderFields[@"Content-Encoding"] isEqualToString:@"gzip"]) {
      request.HTTPBody = RCTGzipData(request.HTTPBody, -1 /* default */);
      [request setValue:(@(request.HTTPBody.length)).description forHTTPHeaderField:@"Content-Length"];
    }

    dispatch_async(self->_methodQueue, ^{
      block(request);
    });

    return (RCTURLRequestCancellationBlock)nil;
  }];
}

- (BOOL)canHandleRequest:(NSURLRequest *)request
{
  return [self handlerForRequest:request] != nil;
}

/**
 * Process the 'data' part of an HTTP query.
 *
 * 'data' can be a JSON value of the following forms:
 *
 * - {"string": "..."}: a simple JS string that will be UTF-8 encoded and sent as the body
 *
 * - {"uri": "some-uri://..."}: reference to a system resource, e.g. an image in the asset library
 *
 * - {"formData": [...]}: list of data payloads that will be combined into a multipart/form-data request
 *
 * If successful, the callback be called with a result dictionary containing the following (optional) keys:
 *
 * - @"body" (NSData): the body of the request
 *
 * - @"contentType" (NSString): the content type header of the request
 *
 */
- (RCTURLRequestCancellationBlock)processDataForHTTPQuery:(nullable NSDictionary<NSString *, id> *)query callback:
(RCTURLRequestCancellationBlock (^)(NSError *error, NSDictionary<NSString *, id> *result))callback
{
  RCTAssertThread(_methodQueue, @"processDataForHTTPQuery: must be called on method queue");

  if (!query) {
    return callback(nil, nil);
  }
  NSData *body = [RCTConvert NSData:query[@"string"]];
  if (body) {
    return callback(nil, @{@"body": body});
  }
  NSString *base64String = [RCTConvert NSString:query[@"base64"]];
  if (base64String) {
    NSData *data = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
    return callback(nil, @{@"body": data});
  }
  NSURLRequest *request = [RCTConvert NSURLRequest:query[@"uri"]];
  if (request) {

    __block RCTURLRequestCancellationBlock cancellationBlock = nil;
    RCTNetworkTask *task = [self networkTaskWithRequest:request completionBlock:^(NSURLResponse *response, NSData *data, NSError *error) {
      dispatch_async(self->_methodQueue, ^{
        cancellationBlock = callback(error, data ? @{@"body": data, @"contentType": RCTNullIfNil(response.MIMEType)} : nil);
      });
    }];

    [task start];

    __weak RCTNetworkTask *weakTask = task;
    return ^{
      [weakTask cancel];
      if (cancellationBlock) {
        cancellationBlock();
      }
    };
  }
  NSArray<NSDictionary *> *formData = [RCTConvert NSDictionaryArray:query[@"formData"]];
  if (formData) {
    RCTHTTPFormDataHelper *formDataHelper = [RCTHTTPFormDataHelper new];
    formDataHelper.networker = self;
    return [formDataHelper process:formData callback:callback];
  }
  // Nothing in the data payload, at least nothing we could understand anyway.
  // Ignore and treat it as if it were null.
  return callback(nil, nil);
}

+ (NSString *)decodeTextData:(NSData *)data fromResponse:(NSURLResponse *)response withCarryData:(NSMutableData *)inputCarryData
{
  NSStringEncoding encoding = NSUTF8StringEncoding;
  if (response.textEncodingName) {
    CFStringEncoding cfEncoding = CFStringConvertIANACharSetNameToEncoding((CFStringRef)response.textEncodingName);
    encoding = CFStringConvertEncodingToNSStringEncoding(cfEncoding);
  }

  NSMutableData *currentCarryData = inputCarryData ?: [NSMutableData new];
  [currentCarryData appendData:data];

  // Attempt to decode text
  NSString *encodedResponse = [[NSString alloc] initWithData:currentCarryData encoding:encoding];

  if (!encodedResponse && data.length > 0) {
    if (encoding == NSUTF8StringEncoding && inputCarryData) {
      // If decode failed, we attempt to trim broken character bytes from the data.
      // At this time, only UTF-8 support is enabled. Multibyte encodings, such as UTF-16 and UTF-32, require a lot of additional work
      // to determine wether BOM was included in the first data packet. If so, save it, and attach it to each new data packet. If not,
      // an encoding has to be selected with a suitable byte order (for ARM iOS, it would be little endianness).

      CFStringEncoding cfEncoding = CFStringConvertNSStringEncodingToEncoding(encoding);
      // Taking a single unichar is not good enough, due to Unicode combining character sequences or characters outside the BMP.
      // See https://www.objc.io/issues/9-strings/unicode/#common-pitfalls
      // We'll attempt with a sequence of two characters, the most common combining character sequence and characters outside the BMP (emojis).
      CFIndex maxCharLength = CFStringGetMaximumSizeForEncoding(2, cfEncoding);

      NSUInteger removedBytes = 1;

      while (removedBytes < maxCharLength) {
        encodedResponse = [[NSString alloc] initWithData:[currentCarryData subdataWithRange:NSMakeRange(0, currentCarryData.length - removedBytes)]
                                                encoding:encoding];

        if (encodedResponse != nil) {
          break;
        }

        removedBytes += 1;
      }
    } else {
      // We don't have an encoding, or the encoding is incorrect, so now we try to guess
      [NSString stringEncodingForData:data
                      encodingOptions:@{ NSStringEncodingDetectionSuggestedEncodingsKey: @[ @(encoding) ] }
                      convertedString:&encodedResponse
                  usedLossyConversion:NULL];
    }
  }

  if (inputCarryData) {
    NSUInteger encodedResponseLength = [encodedResponse dataUsingEncoding:encoding].length;
    NSData *newCarryData = [currentCarryData subdataWithRange:NSMakeRange(encodedResponseLength, currentCarryData.length - encodedResponseLength)];
    [inputCarryData setData:newCarryData];
  }

  return encodedResponse;
}

- (void)sendData:(NSData *)data
    responseType:(NSString *)responseType
         forTask:(RCTNetworkTask *)task
{
  RCTAssertThread(_methodQueue, @"sendData: must be called on method queue");

  if (data.length == 0) {
    return;
  }

  NSString *responseString;
  if ([responseType isEqualToString:@"text"]) {
    // No carry storage is required here because the entire data has been loaded.
    responseString = [RCTNetworking decodeTextData:data fromResponse:task.response withCarryData:nil];
    if (!responseString) {
      RCTLogWarn(@"Received data was not a string, or was not a recognised encoding.");
      return;
    }
  } else if ([responseType isEqualToString:@"base64"]) {
    responseString = [data base64EncodedStringWithOptions:0];
  } else {
    RCTLogWarn(@"Invalid responseType: %@", responseType);
    return;
  }

  NSArray<id> *responseJSON = @[task.requestID, responseString];
  [self sendEventWithName:@"didReceiveNetworkData" body:responseJSON];
}

- (void)sendRequest:(NSURLRequest *)request
       responseType:(NSString *)responseType
 incrementalUpdates:(BOOL)incrementalUpdates
     responseSender:(RCTResponseSenderBlock)responseSender
{
  RCTAssertThread(_methodQueue, @"sendRequest: must be called on method queue");
  __weak __typeof(self) weakSelf = self;
  __block RCTNetworkTask *task;
  RCTURLRequestProgressBlock uploadProgressBlock = ^(int64_t progress, int64_t total) {
    NSArray *responseJSON = @[task.requestID, @((double)progress), @((double)total)];
    [weakSelf sendEventWithName:@"didSendNetworkData" body:responseJSON];
  };

  RCTURLRequestResponseBlock responseBlock = ^(NSURLResponse *response) {
    NSDictionary<NSString *, NSString *> *headers;
    NSInteger status;
    if ([response isKindOfClass:[NSHTTPURLResponse class]]) { // Might be a local file request
      NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
      headers = httpResponse.allHeaderFields ?: @{};
      status = httpResponse.statusCode;
    } else {
      headers = response.MIMEType ? @{@"Content-Type": response.MIMEType} : @{};
      status = 200;
    }
    // ======================= GM Crypto Native start =======================
    NSMutableDictionary *mutableDic = headers.mutableCopy;
    if ([weakSelf wb_isResponseNeedDecrypt:response request:task.request responseType:responseType]) {
        // 如果 reponse 对应的需要使用原生解密，则将请求头中的 encrypt 设置为 N。这样在 RN 端就不会再对该数据进行解密了
        [mutableDic setValue:@"N" forKey:@"encrypt"];
    }
    id responseURL = response.URL ? response.URL.absoluteString : [NSNull null];
    NSArray<id> *responseJSON = @[task.requestID, @(status), mutableDic, responseURL];
    [weakSelf sendEventWithName:@"didReceiveNetworkResponse" body:responseJSON];
    // ======================= GM Crypto Native end =======================
  };

  // XHR does not allow you to peek at xhr.response before the response is
  // finished. Only when xhr.responseType is set to ''/'text', consumers may
  // peek at xhr.responseText. So unless the requested responseType is 'text',
  // we only send progress updates and not incremental data updates to JS here.
  RCTURLRequestIncrementalDataBlock incrementalDataBlock = nil;
  RCTURLRequestProgressBlock downloadProgressBlock = nil;
  if (incrementalUpdates) {
    if ([responseType isEqualToString:@"text"]) {

      // We need this to carry over bytes, which could not be decoded into text (such as broken UTF-8 characters).
      // The incremental data block holds the ownership of this object, and will be released upon release of the block.
      NSMutableData *incrementalDataCarry = [NSMutableData new];

      incrementalDataBlock = ^(NSData *data, int64_t progress, int64_t total) {
        NSUInteger initialCarryLength = incrementalDataCarry.length;
        NSString *responseString = [RCTNetworking decodeTextData:data
                                                    fromResponse:task.response
                                                   withCarryData:incrementalDataCarry];
        if (!responseString) {
          RCTLogWarn(@"Received data was not a string, or was not a recognised encoding.");
          return;
        }

        // Update progress to include the previous carry length and reduce the current carry length.
        NSArray<id> *responseJSON = @[task.requestID,
                                      responseString,
                                      @(progress + initialCarryLength - incrementalDataCarry.length),
                                      @(total)];

        [weakSelf sendEventWithName:@"didReceiveNetworkIncrementalData" body:responseJSON];
      };
    } else {
      downloadProgressBlock = ^(int64_t progress, int64_t total) {
        NSArray<id> *responseJSON = @[task.requestID, @(progress), @(total)];
        [weakSelf sendEventWithName:@"didReceiveNetworkDataProgress" body:responseJSON];
      };
    }
  }

  RCTURLRequestCompletionBlock completionBlock =
  ^(NSURLResponse *response, NSData *data, NSError *error) {
    __typeof(self) strongSelf = weakSelf;
    if (!strongSelf) {
      return;
    }

    // Unless we were sending incremental (text) chunks to JS, all along, now
    // is the time to send the request body to JS.
    if (!(incrementalUpdates && [responseType isEqualToString:@"text"])) {
      // ======================= GM Crypto Native start =======================
      NSMutableData *mData = data.mutableCopy;
      if ([weakSelf wb_isResponseNeedDecrypt:response request:task.request responseType:responseType]) {
          mData = [weakSelf wb_decryptResponse:response data:mData];
      }
      [strongSelf sendData:mData responseType:responseType forTask:task];
      // ======================= GM Crypto Native end =======================
    }
    NSArray *responseJSON = @[task.requestID,
                              RCTNullIfNil(error.localizedDescription),
                              error.code == kCFURLErrorTimedOut ? @YES : @NO
                              ];

    [strongSelf sendEventWithName:@"didCompleteNetworkResponse" body:responseJSON];
    [strongSelf->_tasksByRequestID removeObjectForKey:task.requestID];
  };

  task = [self networkTaskWithRequest:request completionBlock:completionBlock];
  task.downloadProgressBlock = downloadProgressBlock;
  task.incrementalDataBlock = incrementalDataBlock;
  task.responseBlock = responseBlock;
  task.uploadProgressBlock = uploadProgressBlock;

  if (task.requestID) {
    if (!_tasksByRequestID) {
      _tasksByRequestID = [NSMutableDictionary new];
    }
    _tasksByRequestID[task.requestID] = task;
    responseSender(@[task.requestID]);
  }

  [task start];
}

#pragma mark - Public API

- (RCTNetworkTask *)networkTaskWithRequest:(NSURLRequest *)request completionBlock:(RCTURLRequestCompletionBlock)completionBlock
{
  id<RCTURLRequestHandler> handler = [self handlerForRequest:request];
  if (!handler) {
    RCTLogError(@"No suitable URL request handler found for %@", request.URL);
    return nil;
  }

  RCTNetworkTask *task = [[RCTNetworkTask alloc] initWithRequest:request
                                                         handler:handler
                                                   callbackQueue:_methodQueue];
  task.completionBlock = completionBlock;
  return task;
}

#pragma mark - JS API

RCT_EXPORT_METHOD(sendRequest:(NSDictionary *)query
                  responseSender:(RCTResponseSenderBlock)responseSender)
{
  // TODO: buildRequest returns a cancellation block, but there's currently
  // no way to invoke it, if, for example the request is cancelled while
  // loading a large file to build the request body
  // ======================= GM Crypto Native start =======================
  if ([self wb_isRequestNeedEncrypt:query]) {
      NSMutableDictionary *mutableQuery = query.mutableCopy;
      [self wb_addSignatureForRequestHeader:mutableQuery];
      query = mutableQuery;
  }
  // ======================= GM Crypto Native end =======================
  [self buildRequest:query completionBlock:^(NSURLRequest *request) {
    NSString *responseType = [RCTConvert NSString:query[@"responseType"]];
    BOOL incrementalUpdates = [RCTConvert BOOL:query[@"incrementalUpdates"]];
    [self sendRequest:request
         responseType:responseType
   incrementalUpdates:incrementalUpdates
       responseSender:responseSender];
  }];
}

RCT_EXPORT_METHOD(abortRequest:(nonnull NSNumber *)requestID)
{
  [_tasksByRequestID[requestID] cancel];
  [_tasksByRequestID removeObjectForKey:requestID];
}

RCT_EXPORT_METHOD(clearCookies:(RCTResponseSenderBlock)responseSender)
{
  NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
  if (!storage.cookies.count) {
    responseSender(@[@NO]);
    return;
  }

  for (NSHTTPCookie *cookie in storage.cookies) {
    [storage deleteCookie:cookie];
  }
  responseSender(@[@YES]);
}

#pragma mark - GM Native Encrypt Related
// ======================= GM Crypto Native start =======================

- (BOOL)wb_isRequestNeedEncrypt:(NSDictionary *)query{
    // 从 header 中判断是否要使用原生加密
    NSDictionary *headers = [RCTConvert NSDictionary:query[@"headers"]];
    NSString *nativeGmEncrypt = [headers objectForKey:NATIVE_GM_ENCRYPT];
    NSString *encrypt = [headers objectForKey:GM_ENCRYPT];
    self.useNativeEncrypt = [nativeGmEncrypt isEqualToString:@"Y"] && [encrypt isEqualToString:@"Y"];
    if (self.useNativeEncrypt && self.wbGMCryptoNativeDelegate) {
        NSString *url = [query valueForKey:@"url"];
        if (url) {
            NSLog(@"GMNative==== url = %@", url);
            [self.wbGMCryptoNativeDelegate wb_sendTestMessageToTianYan:@{@"GMNative==== RequestNeedGMEncryptUrl": url}.mutableCopy];
        }
        return YES;
    } else {
        return NO;
    }
}

// 如果需要进行国密加密，对在请求头中增加国密签名串 signature = xxxx
- (void)wb_addSignatureForRequestHeader:(NSMutableDictionary *)query{
    NSString *method = [RCTConvert NSString:RCTNilIfNull(query[@"method"])].uppercaseString;
    NSMutableDictionary *headers = (NSMutableDictionary *)[RCTConvert NSDictionary:query[@"headers"]];
    if ([method isEqualToString:@"GET"]) {
        NSString *requestURLStr = [RCTConvert NSString:query[@"url"]];
        NSMutableDictionary *paramDic = [self wb_getParamDicWithURLStr:requestURLStr];
        if (!paramDic) {
            return;
        }
        NSString *param = [paramDic objectForKey:@"param"];
        if (!param) {
            return;
        }
        @try {
            NSString *signature = [self.wbGMCryptoNativeDelegate wb_SM2SignatureWithContent:param];
            if (signature) {
                NSString *url = [query valueForKey:@"url"];
                if (url) {
                    [self.wbGMCryptoNativeDelegate wb_sendTestMessageToTianYan:@{@"GMNative==== GetRequestNeedGMEncryptUrl": url, @"signature": signature}.mutableCopy];
                }
                [headers setValue:signature forKey:SIGNATURE];
                [query setValue:headers forKey:@"headers"];
            } else {
                //如果签名失败：1、将请求头中的使用加密和原生加密标识都修改成 N，2、useNativeEncrypt 重置为 false
                [headers setValue:@"N" forKey:NATIVE_GM_ENCRYPT];
                [headers setValue:@"N" forKey:GM_ENCRYPT];
                [query setValue:headers forKey:@"headers"];
                self.useNativeEncrypt = NO;
                [self.wbGMCryptoNativeDelegate wb_reportCustomKeyValueEventProxy:NATIVE_GM_ENCRYPT props:@{@"fail" : @"GMEncrypt GET signature null failure"}];
            }
        } @catch (NSException *exception) {
            NSLog(@"========== %@", [exception reason]);
            [headers setValue:@"N" forKey:NATIVE_GM_ENCRYPT];
            [headers setValue:@"N" forKey:GM_ENCRYPT];
            [query setValue:headers forKey:@"headers"];
            self.useNativeEncrypt = NO;
            NSString *failReason = @"";
            if ([exception reason]) {
                failReason = [NSString stringWithFormat:@"%@",[exception reason]];
            }
            [self.wbGMCryptoNativeDelegate wb_reportCustomKeyValueEventProxy:@"GMEncrypt_GET_signature_failure" props:@{@"fail" : failReason}];
        }
    } else if ([method isEqualToString:@"POST"]) {
        NSDictionary<NSString *, id> *data = [RCTConvert NSDictionary:RCTNilIfNull(query[@"data"])];
        if (![data objectForKey:REQUEST_BODY_KEY_STRING]) {
            return;
        }
        @try {
            NSString *stringValue = [data objectForKey:REQUEST_BODY_KEY_STRING];
            if (stringValue) {
                NSString *signature = [self.wbGMCryptoNativeDelegate wb_SM2SignatureWithContent:stringValue];
                if (signature) {
                    NSString *url = [query valueForKey:@"url"];
                    if (url) {
                        [self.wbGMCryptoNativeDelegate wb_sendTestMessageToTianYan:@{@"GMNative==== PostRequestNeedGMEncryptUrl": url, @"signature": signature}.mutableCopy];
                    }
                    [headers setValue:signature forKey:SIGNATURE];
                    [query setValue:headers forKey:@"headers"];
                } else {
                    //如果签名失败：1、将请求头中的使用加密和原生加密标识都修改成 N，2、useNativeEncrypt 重置为 false
                    [headers setValue:@"N" forKey:NATIVE_GM_ENCRYPT];
                    [headers setValue:@"N" forKey:GM_ENCRYPT];
                    [query setValue:headers forKey:@"headers"];
                    self.useNativeEncrypt = NO;
                    [self.wbGMCryptoNativeDelegate wb_reportCustomKeyValueEventProxy:NATIVE_GM_ENCRYPT props:@{@"fail" : @"GMEncrypt POST signature null failure"}];
                }
            }
        } @catch (NSException *exception) {
            NSLog(@"========== %@", [exception reason]);
            [headers setValue:@"N" forKey:NATIVE_GM_ENCRYPT];
            [headers setValue:@"N" forKey:GM_ENCRYPT];
            [query setValue:headers forKey:@"headers"];
            self.useNativeEncrypt = NO;
            NSString *failReason = @"";
            if ([exception reason]) {
                failReason = [NSString stringWithFormat:@"%@",[exception reason]];
            }
            [self.wbGMCryptoNativeDelegate wb_reportCustomKeyValueEventProxy:@"GMEncrypt_POST_signature_failure" props:@{@"fail" : failReason}];
        }
        
    }
    [self wb_encryptRequestParam:query];
}

// 如果需要进行国密加密，对请求进行加密操作
- (void)wb_encryptRequestParam:(NSMutableDictionary *)query{
    NSString *method = [RCTConvert NSString:RCTNilIfNull(query[@"method"])].uppercaseString;
    NSMutableDictionary *headers = (NSMutableDictionary *)[RCTConvert NSDictionary:query[@"headers"]];
    NSString *requestURLStr = [RCTConvert NSString:query[@"url"]];
    if ([method isEqualToString:@"GET"]) {
        NSMutableDictionary *paramDic = [self wb_getParamDicWithURLStr:requestURLStr];
        if (!paramDic) {
            return;
        }
        NSString *param = [paramDic objectForKey:@"param"];
        if (!param) {
            return;
        }
        // 如果是 get 请求，取出 url 中以 param 为 key 的参数体,将取出的内容进行 SM4 加密转 base64
        @try {
            NSString *encryptContent = [self.wbGMCryptoNativeDelegate wb_SM4EncryptWithContent:param];
            if (encryptContent) {
                NSString *url = [query valueForKey:@"url"];
                if (url) {
                    [self.wbGMCryptoNativeDelegate wb_sendTestMessageToTianYan:@{@"GMNative==== GetRequestNeedGMEncryptUrl": url, @"encryptContent": encryptContent}.mutableCopy];
                }
              // 需要 encode ，否则会出现 + 传到后台为空格的情况
              NSString *encodedString = (NSString *)
                  CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                                            (CFStringRef)encryptContent,
                                                                            NULL,
                                                                            (CFStringRef)@"+",
                                                                            kCFStringEncodingUTF8));
              NSString *newURLStr = [self wb_replaceURL:requestURLStr key:@"param" withValue:encodedString];
              [query setValue:newURLStr forKey:@"url"];
            } else {
                // 如果加密失败，则1、不改变原有url 2、因为加密失败了，请求头中的encrypt改为N，encryptNative改为N，signature删除 3、将useNativeEncrypt改为false
                [headers setValue:@"N" forKey:NATIVE_GM_ENCRYPT];
                [headers setValue:@"N" forKey:GM_ENCRYPT];
                [headers removeObjectForKey:SIGNATURE];
                [query setValue:headers forKey:@"headers"];
                self.useNativeEncrypt = NO;
                [self.wbGMCryptoNativeDelegate wb_reportCustomKeyValueEventProxy:NATIVE_GM_ENCRYPT props:@{@"fail" : @"GMEncrypt GET encrypt null failure"}];
            }
        } @catch (NSException *exception) {
            NSLog(@"========== %@", [exception reason]);
            [headers setValue:@"N" forKey:NATIVE_GM_ENCRYPT];
            [headers setValue:@"N" forKey:GM_ENCRYPT];
            [headers removeObjectForKey:SIGNATURE];
            [query setValue:headers forKey:@"headers"];
            self.useNativeEncrypt = NO;
            NSString *failReason = @"";
            if ([exception reason]) {
                failReason = [NSString stringWithFormat:@"%@",[exception reason]];
            }
            [self.wbGMCryptoNativeDelegate wb_reportCustomKeyValueEventProxy:@"GMEncrypt_GET_encrypt_failure" props:@{@"fail" : failReason}];
        }
    } else if ([method isEqualToString:@"POST"]) {
        NSMutableDictionary<NSString *, id> *data = (NSMutableDictionary *)[RCTConvert NSDictionary:RCTNilIfNull(query[@"data"])];
        if (![data objectForKey:REQUEST_BODY_KEY_STRING]) {
            return;
        }
        @try {
            NSString *stringValue = [data objectForKey:REQUEST_BODY_KEY_STRING];
            NSString *contentType = [headers objectForKey:CONTENT_TYPE_HEADER_NAME];
            if (stringValue && [contentType isEqualToString:@"application/json"]) {
                // 如果是 POST 请求 只针对 body 是 string 类型，且 Content-Type=application/json 的请求进行加密
                NSString *encryptContent = [self.wbGMCryptoNativeDelegate wb_SM4EncryptWithContent:stringValue];
                if (encryptContent) {
                    NSString *url = [query valueForKey:@"url"];
                    if (url) {
                        [self.wbGMCryptoNativeDelegate wb_sendTestMessageToTianYan:@{@"GMNative==== PostRequestNeedGMEncryptUrl": url, @"encryptContent": encryptContent}.mutableCopy];
                    }
                    [data setValue:encryptContent forKey:REQUEST_BODY_KEY_STRING];
                    [query setValue:data forKey:@"data"];
                } else {
                    // 如果加密失败，则1、不改变原有url 2、因为加密失败了，请求头中的encrypt改为N，encryptNative改为N，signature删除 3、将useNativeEncrypt改为false
                    [headers setValue:@"N" forKey:NATIVE_GM_ENCRYPT];
                    [headers setValue:@"N" forKey:GM_ENCRYPT];
                    [headers removeObjectForKey:SIGNATURE];
                    [query setValue:headers forKey:@"headers"];
                    self.useNativeEncrypt = NO;
                    [self.wbGMCryptoNativeDelegate wb_reportCustomKeyValueEventProxy:NATIVE_GM_ENCRYPT props:@{@"fail" : @"GMEncrypt POST encrypt null failure"}];
                }
            }
        } @catch (NSException *exception) {
            NSLog(@"========== %@", [exception reason]);
            [headers setValue:@"N" forKey:NATIVE_GM_ENCRYPT];
            [headers setValue:@"N" forKey:GM_ENCRYPT];
            [headers removeObjectForKey:SIGNATURE];
            [query setValue:headers forKey:@"headers"];
            self.useNativeEncrypt = NO;
            NSString *failReason = @"";
            if ([exception reason]) {
                failReason = [NSString stringWithFormat:@"%@",[exception reason]];
            }
            [self.wbGMCryptoNativeDelegate wb_reportCustomKeyValueEventProxy:@"GMEncrypt_POST_encrypt_failure" props:@{@"fail" : failReason}];
        }
    }
}

- (BOOL)wb_isResponseNeedDecrypt:(NSURLResponse *)response request:(NSURLRequest *)request responseType:(NSString *)responseType{
    NSDictionary *responseHeaderFields = ((NSHTTPURLResponse *)response).allHeaderFields;
    BOOL needDecrypt = [[responseHeaderFields objectForKey:@"encrypt"] isEqualToString:@"Y"];
    NSDictionary *requestHeaderFields = request.allHTTPHeaderFields;
    NSString *nativeGmEncrypt = [requestHeaderFields objectForKey:NATIVE_GM_ENCRYPT];
    NSString *encrypt = [requestHeaderFields objectForKey:GM_ENCRYPT];
    BOOL useNativeEncrypt = [nativeGmEncrypt isEqualToString:@"Y"] && [encrypt isEqualToString:@"Y"];
    if (needDecrypt && useNativeEncrypt && self.wbGMCryptoNativeDelegate && [responseType isEqualToString:@"text"]) {
        if (request.URL.absoluteString) {
            NSLog(@"GMNative==== url NeedDecrypt = %@", request.URL.absoluteString);
            [self.wbGMCryptoNativeDelegate wb_sendTestMessageToTianYan:@{@"GMNative==== ResponseNeedGMDecryptUrl": request.URL.absoluteString}.mutableCopy];
        }
        return YES;
    } else {
        return NO;
    }
}

// 如果需要进行国密加密，对 response 进行解密操作
- (NSMutableData *)wb_decryptResponse:(NSURLResponse *)response
                      data:(NSMutableData *)data{
    NSStringEncoding encoding = NSUTF8StringEncoding;
    if (response.textEncodingName) {
      CFStringEncoding cfEncoding = CFStringConvertIANACharSetNameToEncoding((CFStringRef)response.textEncodingName);
      encoding = CFStringConvertEncodingToNSStringEncoding(cfEncoding);
    }
    NSString *dataStr = [[NSString alloc] initWithData:data encoding:encoding];
    @try {
        NSString *decryptedStr = [self.wbGMCryptoNativeDelegate wb_SM4DecryptWithContent:dataStr];
        if (decryptedStr) {
            NSString *url = response.URL.absoluteString;
            if (url) {
                [self.wbGMCryptoNativeDelegate wb_sendTestMessageToTianYan:@{@"GMNative==== ResponseNeedGMDecryptUrl": url, @"decryptedStr": decryptedStr}.mutableCopy];
            }
            // 在这里给返回的数据增加不需要解密的标识
            NSMutableData *data = (NSMutableData *)[decryptedStr dataUsingEncoding:NSUTF8StringEncoding];
            return data;
        } else {
            [self.wbGMCryptoNativeDelegate wb_reportCustomKeyValueEventProxy:NATIVE_GM_ENCRYPT props:@{@"fail" : @"Decrypt null failure"}];
            return data;
        }
    } @catch (NSException *exception) {
        NSString *failReason = @"";
        if ([exception reason]) {
            failReason = [NSString stringWithFormat:@"%@",[exception reason]];
        }
        [self.wbGMCryptoNativeDelegate wb_reportCustomKeyValueEventProxy:@"GMEncrypt_decrypt_failure" props:@{@"fail" : failReason}];
        return data;
    }
}

// 将 URL 请求中的参数存放到 NSMutableDictionary 中
- (NSMutableDictionary *)wb_getParamDicWithURLStr:(NSString *)urlStr{
    if (!urlStr) {
        return nil;
    }
    NSArray *allParamStrArr = [urlStr componentsSeparatedByString:@"?"];
    if (!(allParamStrArr.count > 1)) {
        return nil;
    }
    NSString *allParamStr = [allParamStrArr objectAtIndex:1];
    //为了避免 base64 里面的=号干扰预先把 == 转换为##
    allParamStr = [allParamStr stringByReplacingOccurrencesOfString:@"==" withString:@"##"];
    NSArray *paramStrArr = [allParamStr componentsSeparatedByString:@"&"];
    NSMutableDictionary *paramDic = [NSMutableDictionary new];
    NSString *key = nil;
    NSString *value = nil;
    for (NSString *kayValueStr in paramStrArr) {
        NSArray *tempArr = [kayValueStr componentsSeparatedByString:@"="];
        if (tempArr.count == 2) {
            key = tempArr[0];
            value = tempArr[1];
        } else {
            if (tempArr.count != 1) {
                continue;
            } else {
                key = tempArr[0];
                value = @"".mutableCopy;
            }
        }
        if (![key isEqualToString:@""]) {
            value = [value stringByReplacingOccurrencesOfString:@"##" withString:@"=="];
            [paramDic setValue:value forKey:key];
        }
    }
    return paramDic;
}

// 将 URL 中的以 key 为名的参数的值替换为 newParamValue，然后返回新的 URL
- (NSString *)wb_replaceURL:(NSString *)urlStr key:(NSString *)paramKey withValue:(NSString *)newParamValue{
    if (!urlStr || !paramKey || !newParamValue) { return urlStr; }
    NSMutableString *mutableUrlStr = urlStr.mutableCopy;
    NSString *subStr = [NSString stringWithFormat:@"%@=", paramKey];
    NSRange range = [mutableUrlStr rangeOfString:subStr];
    if (range.location == NSNotFound) { return mutableUrlStr; }
    NSString *leftStr = [mutableUrlStr substringFromIndex:range.location + range.length];
    NSRange range1 = [leftStr rangeOfString:@"&"];
    NSString *newStr = mutableUrlStr;
    if (range1.location == NSNotFound) {
        NSUInteger newLocation = range.location + range.length;
        NSUInteger newLength = mutableUrlStr.length - (range.location + range.length);
        NSRange newRange = NSMakeRange(newLocation, newLength);
        newStr = [mutableUrlStr stringByReplacingCharactersInRange:newRange withString:newParamValue];
    } else {
        NSRange newRange = NSMakeRange(range.location + range.length, range1.location);
        newStr = [mutableUrlStr stringByReplacingCharactersInRange:newRange withString:newParamValue];
    }
    return newStr;
}
 
// ======================= GM Crypto Native end =======================

@end

@implementation RCTBridge (RCTNetworking)

- (RCTNetworking *)networking
{
  return [self moduleForClass:[RCTNetworking class]];
}

@end
