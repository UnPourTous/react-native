package com.facebook.react.modules.network;

import okhttp3.OkHttpClient;

public interface GmNativeEncryptInterface{
  public String SMSignature(String source);
  public String SM4Encrypt(String source);
  public String SM4Decrypt(String cipher);
}
