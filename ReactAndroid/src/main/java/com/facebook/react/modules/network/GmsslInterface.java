package com.facebook.react.modules.network;

import okhttp3.OkHttpClient;

public interface GmsslInterface{
  public void setGmsslFactory(OkHttpClient.Builder clientBuilder);
}
