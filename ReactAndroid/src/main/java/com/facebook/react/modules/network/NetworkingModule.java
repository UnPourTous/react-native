/**
 * Copyright (c) 2015-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

package com.facebook.react.modules.network;

import javax.annotation.Nullable;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.net.URLEncoder;
import java.net.URLDecoder;
import android.util.Log;
import android.text.TextUtils;

import android.util.Base64;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.GuardedAsyncTask;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.common.network.OkHttpCallUtil;
import com.facebook.react.module.annotations.ReactModule;
import com.facebook.react.modules.core.DeviceEventManagerModule.RCTDeviceEventEmitter;
import com.facebook.react.modules.network.GmsslInterface;
import com.facebook.react.modules.network.GmNativeEncryptInterface;
import com.facebook.react.modules.network.StaticsInterface;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.CookieJar;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.JavaNetCookieJar;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.ByteString;

/**
 * Implements the XMLHttpRequest JavaScript interface.
 */
@ReactModule(name = NetworkingModule.NAME)
public final class NetworkingModule extends ReactContextBaseJavaModule {

  protected static final String NAME = "Networking";

  private static final String CONTENT_ENCODING_HEADER_NAME = "content-encoding";
  private static final String CONTENT_TYPE_HEADER_NAME = "content-type";
  private static final String REQUEST_BODY_KEY_STRING = "string";
  private static final String REQUEST_BODY_KEY_URI = "uri";
  private static final String REQUEST_BODY_KEY_FORMDATA = "formData";
  private static final String REQUEST_BODY_KEY_BASE64 = "base64";
  private static final String USER_AGENT_HEADER_NAME = "user-agent";
  private static final int CHUNK_TIMEOUT_NS = 100 * 1000000; // 100ms
  private static final int MAX_CHUNK_SIZE_BETWEEN_FLUSHES = 8 * 1024; // 8K
  private static final long CONNECT_TIMEOUT_MS = 10000; // 10ms
  private static final long TRANSPORT_TIMEOUT_MS = 30000; // 30ms
  private static final String USE_GMSSL = "use-gmssl";
  private static final String NATIVE_GM_ENCRYPT = "native-gm-encrypt";
  private static final String GM_ENCRYPT = "encrypt";
  private static final String SIGNATURE = "signature";
  private final OkHttpClient mClient;
  private final ForwardingCookieHandler mCookieHandler;
  private final @Nullable String mDefaultUserAgent;
  private final CookieJarContainer mCookieJarContainer;
  private final Set<Integer> mRequestIds;
  private boolean mShuttingDown;
  private GmsslInterface mGmsslImpl;
  private GmNativeEncryptInterface mGmEncryptImpl;
  private StaticsInterface mStaticsInterfaceImpl;

  /* package */ NetworkingModule(
      ReactApplicationContext reactContext,
      @Nullable String defaultUserAgent,
      OkHttpClient client,
      @Nullable List<NetworkInterceptorCreator> networkInterceptorCreators) {
    super(reactContext);

    if (networkInterceptorCreators != null) {
      OkHttpClient.Builder clientBuilder = client.newBuilder();
      for (NetworkInterceptorCreator networkInterceptorCreator : networkInterceptorCreators) {
        clientBuilder.addNetworkInterceptor(networkInterceptorCreator.create());
      }
      client = clientBuilder.build();
    }
    mClient = client;
    mCookieHandler = new ForwardingCookieHandler(reactContext);
    mCookieJarContainer = (CookieJarContainer) mClient.cookieJar();
    mShuttingDown = false;
    mDefaultUserAgent = defaultUserAgent;
    mRequestIds = new HashSet<>();
  }

  /**
   * @param context the ReactContext of the application
   * @param defaultUserAgent the User-Agent header that will be set for all requests where the
   * caller does not provide one explicitly
   * @param client the {@link OkHttpClient} to be used for networking
   */
  /* package */ NetworkingModule(
    ReactApplicationContext context,
    @Nullable String defaultUserAgent,
    OkHttpClient client) {
    this(context, defaultUserAgent, client, null);
  }

  /**
   * @param context the ReactContext of the application
   */
  public NetworkingModule(final ReactApplicationContext context) {
    this(context, null, OkHttpClientProvider.createClient(), null);
  }

  /**
   * @param context the ReactContext of the application
   * @param networkInterceptorCreators list of {@link NetworkInterceptorCreator}'s whose create()
   * methods would be called to attach the interceptors to the client.
   */
  public NetworkingModule(
    ReactApplicationContext context,
    List<NetworkInterceptorCreator> networkInterceptorCreators) {
    this(context, null, OkHttpClientProvider.createClient(), networkInterceptorCreators);
  }

  /**
   * @param context the ReactContext of the application
   * @param defaultUserAgent the User-Agent header that will be set for all requests where the
   * caller does not provide one explicitly
   */
  public NetworkingModule(ReactApplicationContext context, String defaultUserAgent) {
    this(context, defaultUserAgent, OkHttpClientProvider.createClient(), null);
  }

  @Override
  public void initialize() {
    mCookieJarContainer.setCookieJar(new JavaNetCookieJar(mCookieHandler));
  }

  @Override
  public String getName() {
    return NAME;
  }

  @Override
  public void onCatalystInstanceDestroy() {
    mShuttingDown = true;
    cancelAllRequests();

    mCookieHandler.destroy();
    mCookieJarContainer.removeCookieJar();
  }

  @ReactMethod
  /**
   * @param timeout value of 0 results in no timeout
   */
  public void sendRequest(
      String method,
      String url,
      final int requestId,
      ReadableArray headers,
      ReadableMap data,
      final String responseType,
      final boolean useIncrementalUpdates,
      int timeout,
      boolean withCredentials) {
    Request.Builder requestBuilder = new Request.Builder().url(url);

    if (requestId != 0) {
      requestBuilder.tag(requestId);
    }

    final RCTDeviceEventEmitter eventEmitter = getEventEmitter();
    OkHttpClient.Builder clientBuilder = mClient.newBuilder();
    //从header中增加参数区分是否使用国密
    Headers checkGmHeader = extractHeaders(headers, data);
    boolean useGmssl = "Y".equals(checkGmHeader.get(USE_GMSSL));
    if (useGmssl && mGmsslImpl != null) {
      //gmssl
      mGmsslImpl.setGmsslFactory(clientBuilder);

    }
    if (!withCredentials) {
      clientBuilder.cookieJar(CookieJar.NO_COOKIES);
    }

    // If JS is listening for progress updates, install a ProgressResponseBody that intercepts the
    // response and counts bytes received.
    if (useIncrementalUpdates) {
      clientBuilder.addNetworkInterceptor(new Interceptor() {
        @Override
        public Response intercept(Interceptor.Chain chain) throws IOException {
          Response originalResponse = chain.proceed(chain.request());
          ProgressResponseBody responseBody = new ProgressResponseBody(
            originalResponse.body(),
            new ProgressListener() {
              long last = System.nanoTime();

              @Override
              public void onProgress(long bytesWritten, long contentLength, boolean done) {
                long now = System.nanoTime();
                if (!done && !shouldDispatch(now, last)) {
                  return;
                }
                if (responseType.equals("text")) {
                  // For 'text' responses we continuously send response data with progress info to
                  // JS below, so no need to do anything here.
                  return;
                }
                ResponseUtil.onDataReceivedProgress(
                  eventEmitter,
                  requestId,
                  bytesWritten,
                  contentLength);
                last = now;
              }
            });
          return originalResponse.newBuilder().body(responseBody).build();
        }
      });
    }

    // Set timeout for new client.
    // read timeout could be customized.
    // See https://www.baeldung.com/okhttp-timeouts
    clientBuilder
      .connectTimeout(CONNECT_TIMEOUT_MS, TimeUnit.MILLISECONDS)
      .readTimeout(timeout, TimeUnit.MILLISECONDS)
      .writeTimeout(TRANSPORT_TIMEOUT_MS, TimeUnit.MILLISECONDS);
    OkHttpClient client = clientBuilder.build();

    Headers requestHeaders = extractHeaders(headers, data);
    if (requestHeaders == null) {
      ResponseUtil.onRequestError(eventEmitter, requestId, "Unrecognized headers format", null);
      return;
    }
    //从header中判断是否要使用原生加密
    boolean useNativeEncrypt = "Y".equals(requestHeaders.get(NATIVE_GM_ENCRYPT)) && "Y".equals(requestHeaders.get(GM_ENCRYPT));
    //如果需要进行国密加密,对在请求头中增加国密签名串 signature =xxxx
    //如果签名失败，1、将请求头中的使用加密和原生加密标识都修改成N，2、useNativeEncrypt重置为false
    if (useNativeEncrypt && mGmEncryptImpl != null) {
      if ("GET".equals(method)) {
        String param = urlSplit(url).get("param");
        String signature = "";
        try  {
          String decodeParam = URLDecoder.decode(param,"UTF-8");
          signature = mGmEncryptImpl.SMSignature(decodeParam);
        } catch (Exception e) {
          if (mStaticsInterfaceImpl != null) {
            mStaticsInterfaceImpl.reportEvent(NATIVE_GM_ENCRYPT,"GMEncrypt GET signature decode exception");
          }
        }
        if (!TextUtils.isEmpty(signature)) {
          requestHeaders = requestHeaders.newBuilder().add(SIGNATURE,signature).build();
        } else {
          requestHeaders = requestHeaders.newBuilder().set(GM_ENCRYPT,"N").set(NATIVE_GM_ENCRYPT,"N").build();
          useNativeEncrypt = false;
          if (mStaticsInterfaceImpl != null) {
            mStaticsInterfaceImpl.reportEvent(NATIVE_GM_ENCRYPT,"GMEncrypt GET signature null failure");
          }
        }
      } else if ("POST".equals(method)) {
        if (data.hasKey(REQUEST_BODY_KEY_STRING)){
          String body = data.getString(REQUEST_BODY_KEY_STRING);
          String signature = mGmEncryptImpl.SMSignature(data.getString(REQUEST_BODY_KEY_STRING));
          if (!TextUtils.isEmpty(signature)) {
            requestHeaders = requestHeaders.newBuilder().add(SIGNATURE,signature).build();
          } else {
            requestHeaders = requestHeaders.newBuilder().set(GM_ENCRYPT,"N").set(NATIVE_GM_ENCRYPT,"N").build();
            useNativeEncrypt = false;
            if (mStaticsInterfaceImpl != null) {
              mStaticsInterfaceImpl.reportEvent(NATIVE_GM_ENCRYPT,"GMEncrypt POST signature null failure");
            }
          }
        }
      }

    }
    String contentType = requestHeaders.get(CONTENT_TYPE_HEADER_NAME);
    String contentEncoding = requestHeaders.get(CONTENT_ENCODING_HEADER_NAME);
    requestBuilder.headers(requestHeaders);
    //如果需要进行国密加密,对请求进行加密操作
    if (useNativeEncrypt && mGmEncryptImpl != null && "GET".equals(method)) {
      //如果是get请求，取出url中以param为key的参数体,将取出的内容进行SM4加密转base64，加密后的内容,做一次URLEncode塞回到原先的url中做为param的新value
      //如果加密失败，则1、不改变原有url 2、因为加密失败了，请求头中的encrypt改为N，encryptNative改为N，signature删除 3、将useNativeEncrypt改为false
      String param = urlSplit(url).get("param");
      String encryptContent = "";
      try  {
        String decodeParam = URLDecoder.decode(param,"UTF-8");
        encryptContent = mGmEncryptImpl.SM4Encrypt(decodeParam);
      } catch (Exception e) {
        if (mStaticsInterfaceImpl != null) {
          mStaticsInterfaceImpl.reportEvent(NATIVE_GM_ENCRYPT,"GMEncrypt GET encrypt decode exception");
        }
      }
      if(!TextUtils.isEmpty(encryptContent)){
        try {
          String encodeContent = URLEncoder.encode(encryptContent, "UTF-8");
          String newUrl = replaceUrlParam(url,"param",encodeContent);
          requestBuilder.url(newUrl);
        } catch (Exception e) {
          requestBuilder.header(GM_ENCRYPT,"N").header(NATIVE_GM_ENCRYPT,"N").removeHeader(SIGNATURE);
          useNativeEncrypt = false;
          if (mStaticsInterfaceImpl != null) {
            mStaticsInterfaceImpl.reportEvent(NATIVE_GM_ENCRYPT,"GMEncrypt GET encrypt failure Exception = " + e.getMessage());
          }
        }
      } else {
        requestBuilder.header(GM_ENCRYPT,"N").header(NATIVE_GM_ENCRYPT,"N").removeHeader(SIGNATURE);
        useNativeEncrypt = false;
        if (mStaticsInterfaceImpl != null) {
          mStaticsInterfaceImpl.reportEvent(NATIVE_GM_ENCRYPT,"GMEncrypt GET encrypt null failure");
        }
      }
    }
    if (data == null) {
      requestBuilder.method(method, RequestBodyUtil.getEmptyBody(method));
    } else if (data.hasKey(REQUEST_BODY_KEY_STRING)) {
      if (contentType == null) {
        ResponseUtil.onRequestError(
          eventEmitter,
          requestId,
          "Payload is set but no content-type header specified",
          null);
        return;
      }
      String body = data.getString(REQUEST_BODY_KEY_STRING);
      MediaType contentMediaType = MediaType.parse(contentType);
      if (RequestBodyUtil.isGzipEncoding(contentEncoding)) {
        RequestBody requestBody = RequestBodyUtil.createGzip(contentMediaType, body);
        if (requestBody == null) {
          ResponseUtil.onRequestError(eventEmitter, requestId, "Failed to gzip request body", null);
          return;
        }
        requestBuilder.method(method, requestBody);
      } else {
        //如果需要进行国密加密,对请求进行加密操作,如果是POST请求 只针对body是string类型，且Content-Type=application/json的请求进行加密
        if (useNativeEncrypt && mGmEncryptImpl != null && "POST".equals(method)) {
          //如果是post请求，判断请求头中的Content-Type是否是application/json,如果是，将整个requestBody转成字符串，然后进行SM4加密转base64，加密后的内容做为新的请求ResponseBody
          //如果加密失败，则1、不改变原有body 2、因为加密失败了，请求头中的encrypt改为N，encryptNative改为N，signature删除 3、将useNativeEncrypt改为false
          if ("application/json".equals(contentType)) {
            String encryptContent = mGmEncryptImpl.SM4Encrypt(body);
            if (!TextUtils.isEmpty(encryptContent)){
              body = encryptContent;
            } else {
              requestBuilder.header(GM_ENCRYPT,"N").header(NATIVE_GM_ENCRYPT,"N").removeHeader(SIGNATURE);
              useNativeEncrypt = false;
              if (mStaticsInterfaceImpl != null) {
                mStaticsInterfaceImpl.reportEvent(NATIVE_GM_ENCRYPT,"GMEncrypt POST encrypt null failure");
              }
            }
          }
        }
        requestBuilder.method(method, RequestBody.create(contentMediaType, body));
      }
    } else if (data.hasKey(REQUEST_BODY_KEY_BASE64)) {
      if (contentType == null) {
        ResponseUtil.onRequestError(
          eventEmitter,
          requestId,
          "Payload is set but no content-type header specified",
          null);
        return;
      }
      String base64String = data.getString(REQUEST_BODY_KEY_BASE64);
      MediaType contentMediaType = MediaType.parse(contentType);
      requestBuilder.method(
        method,
        RequestBody.create(contentMediaType, ByteString.decodeBase64(base64String)));
    } else if (data.hasKey(REQUEST_BODY_KEY_URI)) {
      if (contentType == null) {
        ResponseUtil.onRequestError(
          eventEmitter,
          requestId,
          "Payload is set but no content-type header specified",
          null);
        return;
      }
      String uri = data.getString(REQUEST_BODY_KEY_URI);
      InputStream fileInputStream =
          RequestBodyUtil.getFileInputStream(getReactApplicationContext(), uri);
      if (fileInputStream == null) {
        ResponseUtil.onRequestError(
          eventEmitter,
          requestId,
          "Could not retrieve file for uri " + uri,
          null);
        return;
      }
      requestBuilder.method(
          method,
          RequestBodyUtil.create(MediaType.parse(contentType), fileInputStream));
    } else if (data.hasKey(REQUEST_BODY_KEY_FORMDATA)) {
      if (contentType == null) {
        contentType = "multipart/form-data";
      }
      ReadableArray parts = data.getArray(REQUEST_BODY_KEY_FORMDATA);
      MultipartBody.Builder multipartBuilder =
          constructMultipartBody(parts, contentType, requestId);
      if (multipartBuilder == null) {
        return;
      }

      requestBuilder.method(
        method,
        RequestBodyUtil.createProgressRequest(
          multipartBuilder.build(),
          new ProgressListener() {
        long last = System.nanoTime();

        @Override
        public void onProgress(long bytesWritten, long contentLength, boolean done) {
          long now = System.nanoTime();
          if (done || shouldDispatch(now, last)) {
            ResponseUtil.onDataSend(eventEmitter, requestId, bytesWritten, contentLength);
            last = now;
          }
        }
      }));
    } else {
      // Nothing in data payload, at least nothing we could understand anyway.
      requestBuilder.method(method, RequestBodyUtil.getEmptyBody(method));
    }

    addRequest(requestId);
    client.newCall(requestBuilder.build()).enqueue(
        new Callback() {
          @Override
          public void onFailure(Call call, IOException e) {
            if (mShuttingDown) {
              return;
            }
            removeRequest(requestId);
            String errorMessage = e.getMessage() != null
                    ? e.getMessage()
                    : "Error while executing request: " + e.getClass().getSimpleName();
            ResponseUtil.onRequestError(eventEmitter, requestId, errorMessage, e);
          }

          @Override
          public void onResponse(Call call, Response response) throws IOException {
            if (mShuttingDown) {
              return;
            }
            removeRequest(requestId);
            //如果需要进行国密加密，且对response进行解密操作,如果解密成功
            String decryptContent = "";
            boolean needDecrypt = "Y".equals(response.header("encrypt","N"));
            Request originalRequest = call.request();
            boolean useNativeDecrypt = false;
            useNativeDecrypt = "Y".equals(originalRequest.header(NATIVE_GM_ENCRYPT)) && "Y".equals(originalRequest.header(GM_ENCRYPT));
            if (useNativeDecrypt && mGmEncryptImpl != null && needDecrypt) {
              if (responseType.equals("text")) {
                try {
                  ResponseBody toDecryptResponseBody = response.peekBody(Long.MAX_VALUE);
                  String toDecryptResponseString = toDecryptResponseBody.string();
                  decryptContent = mGmEncryptImpl.SM4Decrypt(toDecryptResponseString);
                  if (!TextUtils.isEmpty(decryptContent)) {
                    //修改repsonseHeader中encrypt标志，改为N
                    Response.Builder responseBuilder = response.newBuilder();
                    responseBuilder.header(GM_ENCRYPT,"N");
                    response = responseBuilder.build();
                  } else {
                    //解密后的字符串有问题
                    if (mStaticsInterfaceImpl != null) {
                      mStaticsInterfaceImpl.reportEvent(NATIVE_GM_ENCRYPT,"GMEncrypt decrypt decryptContent null failure");
                    }
                  }
                } catch (Exception e) {
                  if (mStaticsInterfaceImpl != null) {
                    mStaticsInterfaceImpl.reportEvent(NATIVE_GM_ENCRYPT,"GMEncrypt decrypt decryptContent exception e = " + e.getMessage());
                  }
                }
              }


            }
            // Before we touch the body send headers to JS
            ResponseUtil.onResponseReceived(
              eventEmitter,
              requestId,
              response.code(),
              translateHeaders(response.headers()),
              response.request().url().toString());

            ResponseBody responseBody = response.body();
            try {
              // If JS wants progress updates during the download, and it requested a text response,
              // periodically send response data updates to JS.
              if (useIncrementalUpdates && responseType.equals("text")) {
                readWithProgress(eventEmitter, requestId, responseBody);
                ResponseUtil.onRequestSuccess(eventEmitter, requestId);
                return;
              }

              // Otherwise send the data in one big chunk, in the format that JS requested.
              String responseString = "";
              if (responseType.equals("text")) {
                try {
                  responseString = responseBody.string();
                  if (!TextUtils.isEmpty(decryptContent) && useNativeDecrypt && needDecrypt){
                    responseString = decryptContent;
                  }
                } catch (IOException e) {
                  if (response.request().method().equalsIgnoreCase("HEAD")) {
                    // The request is an `HEAD` and the body is empty,
                    // the OkHttp will produce an exception.
                    // Ignore the exception to not invalidate the request in the
                    // Javascript layer.
                    // Introduced to fix issue #7463.
                  } else {
                    ResponseUtil.onRequestError(eventEmitter, requestId, e.getMessage(), e);
                  }
                }
              } else if (responseType.equals("base64")) {
                responseString = Base64.encodeToString(responseBody.bytes(), Base64.NO_WRAP);
              }
              ResponseUtil.onDataReceived(eventEmitter, requestId, responseString);
              ResponseUtil.onRequestSuccess(eventEmitter, requestId);
            } catch (IOException e) {
              ResponseUtil.onRequestError(eventEmitter, requestId, e.getMessage(), e);
            }
          }
        });
  }

  private void readWithProgress(
      RCTDeviceEventEmitter eventEmitter,
      int requestId,
      ResponseBody responseBody) throws IOException {
    long totalBytesRead = -1;
    long contentLength = -1;
    try {
      ProgressResponseBody progressResponseBody = (ProgressResponseBody) responseBody;
      totalBytesRead = progressResponseBody.totalBytesRead();
      contentLength = progressResponseBody.contentLength();
    } catch (ClassCastException e) {
      // Ignore
    }

    Reader reader = responseBody.charStream();
    try {
      char[] buffer = new char[MAX_CHUNK_SIZE_BETWEEN_FLUSHES];
      int read;
      while ((read = reader.read(buffer)) != -1) {
        ResponseUtil.onIncrementalDataReceived(
          eventEmitter,
          requestId,
          new String(buffer, 0, read),
          totalBytesRead,
          contentLength);
      }
    } finally {
      reader.close();
    }
  }

  private static boolean shouldDispatch(long now, long last) {
    return last + CHUNK_TIMEOUT_NS < now;
  }

  private synchronized void addRequest(int requestId) {
    mRequestIds.add(requestId);
  }

  private synchronized void removeRequest(int requestId) {
    mRequestIds.remove(requestId);
  }

  private synchronized void cancelAllRequests() {
    for (Integer requestId : mRequestIds) {
      cancelRequest(requestId);
    }
    mRequestIds.clear();
  }

  private static WritableMap translateHeaders(Headers headers) {
    WritableMap responseHeaders = Arguments.createMap();
    for (int i = 0; i < headers.size(); i++) {
      String headerName = headers.name(i);
      // multiple values for the same header
      if (responseHeaders.hasKey(headerName)) {
        responseHeaders.putString(
            headerName,
            responseHeaders.getString(headerName) + ", " + headers.value(i));
      } else {
        responseHeaders.putString(headerName, headers.value(i));
      }
    }
    return responseHeaders;
  }

  @ReactMethod
  public void abortRequest(final int requestId) {
    cancelRequest(requestId);
    removeRequest(requestId);
  }

  private void cancelRequest(final int requestId) {
    // We have to use AsyncTask since this might trigger a NetworkOnMainThreadException, this is an
    // open issue on OkHttp: https://github.com/square/okhttp/issues/869
    new GuardedAsyncTask<Void, Void>(getReactApplicationContext()) {
      @Override
      protected void doInBackgroundGuarded(Void... params) {
        OkHttpCallUtil.cancelTag(mClient, Integer.valueOf(requestId));
      }
    }.execute();
  }

  @ReactMethod
  public void clearCookies(com.facebook.react.bridge.Callback callback) {
    mCookieHandler.clearCookies(callback);
  }

  private @Nullable MultipartBody.Builder constructMultipartBody(
      ReadableArray body,
      String contentType,
      int requestId) {
    RCTDeviceEventEmitter eventEmitter = getEventEmitter();
    MultipartBody.Builder multipartBuilder = new MultipartBody.Builder();
    multipartBuilder.setType(MediaType.parse(contentType));

    for (int i = 0, size = body.size(); i < size; i++) {
      ReadableMap bodyPart = body.getMap(i);

      // Determine part's content type.
      ReadableArray headersArray = bodyPart.getArray("headers");
      Headers headers = extractHeaders(headersArray, null);
      if (headers == null) {
        ResponseUtil.onRequestError(
          eventEmitter,
          requestId,
          "Missing or invalid header format for FormData part.",
          null);
        return null;
      }
      MediaType partContentType = null;
      String partContentTypeStr = headers.get(CONTENT_TYPE_HEADER_NAME);
      if (partContentTypeStr != null) {
        partContentType = MediaType.parse(partContentTypeStr);
        // Remove the content-type header because MultipartBuilder gets it explicitly as an
        // argument and doesn't expect it in the headers array.
        headers = headers.newBuilder().removeAll(CONTENT_TYPE_HEADER_NAME).build();
      }

      if (bodyPart.hasKey(REQUEST_BODY_KEY_STRING)) {
        String bodyValue = bodyPart.getString(REQUEST_BODY_KEY_STRING);
        multipartBuilder.addPart(headers, RequestBody.create(partContentType, bodyValue));
      } else if (bodyPart.hasKey(REQUEST_BODY_KEY_URI)) {
        if (partContentType == null) {
          ResponseUtil.onRequestError(
            eventEmitter,
            requestId,
            "Binary FormData part needs a content-type header.",
            null);
          return null;
        }
        String fileContentUriStr = bodyPart.getString(REQUEST_BODY_KEY_URI);
        InputStream fileInputStream =
            RequestBodyUtil.getFileInputStream(getReactApplicationContext(), fileContentUriStr);
        if (fileInputStream == null) {
          ResponseUtil.onRequestError(
            eventEmitter,
            requestId,
            "Could not retrieve file for uri " + fileContentUriStr,
            null);
          return null;
        }
        multipartBuilder.addPart(headers, RequestBodyUtil.create(partContentType, fileInputStream));
      } else {
        ResponseUtil.onRequestError(eventEmitter, requestId, "Unrecognized FormData part.", null);
      }
    }
    return multipartBuilder;
  }

  /**
   * Extracts the headers from the Array. If the format is invalid, this method will return null.
   */
  private @Nullable Headers extractHeaders(
      @Nullable ReadableArray headersArray,
      @Nullable ReadableMap requestData) {
    if (headersArray == null) {
      return null;
    }
    Headers.Builder headersBuilder = new Headers.Builder();
    for (int headersIdx = 0, size = headersArray.size(); headersIdx < size; headersIdx++) {
      ReadableArray header = headersArray.getArray(headersIdx);
      if (header == null || header.size() != 2) {
        return null;
      }
      String headerName = header.getString(0);
      String headerValue = header.getString(1);
      if (headerName == null || headerValue == null) {
        return null;
      }
      headersBuilder.add(headerName, headerValue);
    }
    if (headersBuilder.get(USER_AGENT_HEADER_NAME) == null && mDefaultUserAgent != null) {
      headersBuilder.add(USER_AGENT_HEADER_NAME, mDefaultUserAgent);
    }

    // Sanitize content encoding header, supported only when request specify payload as string
    boolean isGzipSupported = requestData != null && requestData.hasKey(REQUEST_BODY_KEY_STRING);
    if (!isGzipSupported) {
      headersBuilder.removeAll(CONTENT_ENCODING_HEADER_NAME);
    }

    return headersBuilder.build();
  }

  private RCTDeviceEventEmitter getEventEmitter() {
    return getReactApplicationContext().getJSModule(RCTDeviceEventEmitter.class);
  }

  public void setGmsslImpl(GmsslInterface gmsslImpl){
    this.mGmsslImpl = gmsslImpl;
  }

  public void setGmEncryptImpl(GmNativeEncryptInterface gmEncryptImpl){
    this.mGmEncryptImpl = gmEncryptImpl;
  }

  public void setStaticsInterface(StaticsInterface staticsInterface){
    this.mStaticsInterfaceImpl = staticsInterface;
  }

  /**
   * 将参数存入map集合
   * @param url  url地址
   * @return url请求参数部分存入map集合
   */
  private Map<String, String> urlSplit(String url) {
    Map<String, String> mapRequest = new HashMap<String, String>();
    String[] arrSplit = null;
    String strUrlParam = truncateUrlPage(url);
    if (strUrlParam == null) {
      return mapRequest;
    }
    mapRequest.putAll(convertParamsString2Map(strUrlParam));
    return mapRequest;
  }

  /**
   * 去掉url中的路径，留下请求参数部分
   * @param url url地址
   * @return url请求参数部分
   */
  private String truncateUrlPage(String url) {
    String strAllParam = null;
    String[] arrSplit = null;
    url = url.trim();
    arrSplit = url.split("[?]");
    if (url.length() > 1) {
      if (arrSplit.length > 1) {
        for (int i = 1; i < arrSplit.length; i++) {
          strAllParam = arrSplit[i];
        }
      }
    }
    return strAllParam;
  }

  private Map<String, String> convertParamsString2Map(String paramsString) {
    //为了避免base64 里面的=号干扰预先把== 转换为##
    paramsString= paramsString.replace("==","##");
    Map<String, String> paramsMap = new HashMap<String, String>();
    String[] tempParams = paramsString.split("&");
    String name = null;
    String value = null;
    if (tempParams != null && tempParams.length > 0) {
      for (int i = 0; i < tempParams.length; i++) {
        String[] tempArray = tempParams[i].split("=");
        if (tempArray.length == 2) {
          name = tempArray[0];
          value = tempArray[1];
        } else {
          if (tempArray.length != 1)
            continue;
          else {
            name = tempArray[0];
            value = "";
          }
        }
        if (name!="") {
          value = value.replace("##","==");
          paramsMap.put(name, value);
        }
      }
    }
    return paramsMap;
  }

  private String replaceUrlParam(String url, String paramName, String paramValue) {
    if (!TextUtils.isEmpty(url) && !TextUtils.isEmpty(paramValue) && !TextUtils.isEmpty(paramName)) {
      int index = url.indexOf(paramName + "=");
      if (index != -1) {
        StringBuilder sb = new StringBuilder();
        sb.append(url.substring(0, index)).append(paramName).append("=").append(paramValue);
        int idx = url.indexOf("&", index);
        if (idx != -1) {
          sb.append(url.substring(idx));
        }
        url = sb.toString();
      }
    }
    return url;
  }

}
